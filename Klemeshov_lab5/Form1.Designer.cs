﻿
namespace Klemeshov_lab5
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.storesBtn = new System.Windows.Forms.Button();
            this.workersBtn = new System.Windows.Forms.Button();
            this.applicationsBtn = new System.Windows.Forms.Button();
            this.coursesBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.report1Btn = new System.Windows.Forms.Button();
            this.report2Btn = new System.Windows.Forms.Button();
            this.report3Btn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // storesBtn
            // 
            this.storesBtn.Location = new System.Drawing.Point(28, 44);
            this.storesBtn.Name = "storesBtn";
            this.storesBtn.Size = new System.Drawing.Size(85, 23);
            this.storesBtn.TabIndex = 1;
            this.storesBtn.Text = "stores";
            this.storesBtn.UseVisualStyleBackColor = true;
            this.storesBtn.Click += new System.EventHandler(this.storesBtn_Click);
            // 
            // workersBtn
            // 
            this.workersBtn.Location = new System.Drawing.Point(28, 86);
            this.workersBtn.Name = "workersBtn";
            this.workersBtn.Size = new System.Drawing.Size(85, 23);
            this.workersBtn.TabIndex = 2;
            this.workersBtn.Text = "workers";
            this.workersBtn.UseVisualStyleBackColor = true;
            this.workersBtn.Click += new System.EventHandler(this.workersBtn_Click);
            // 
            // applicationsBtn
            // 
            this.applicationsBtn.Location = new System.Drawing.Point(28, 133);
            this.applicationsBtn.Name = "applicationsBtn";
            this.applicationsBtn.Size = new System.Drawing.Size(85, 23);
            this.applicationsBtn.TabIndex = 3;
            this.applicationsBtn.Text = "applications";
            this.applicationsBtn.UseVisualStyleBackColor = true;
            this.applicationsBtn.Click += new System.EventHandler(this.applicationsBtn_Click);
            // 
            // coursesBtn
            // 
            this.coursesBtn.Location = new System.Drawing.Point(28, 174);
            this.coursesBtn.Name = "coursesBtn";
            this.coursesBtn.Size = new System.Drawing.Size(85, 23);
            this.coursesBtn.TabIndex = 4;
            this.coursesBtn.Text = "courses";
            this.coursesBtn.UseVisualStyleBackColor = true;
            this.coursesBtn.Click += new System.EventHandler(this.coursesBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(24, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(232, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "Лабораторна робота №5";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Клемешов Денис";
            // 
            // report1Btn
            // 
            this.report1Btn.Location = new System.Drawing.Point(33, 30);
            this.report1Btn.Name = "report1Btn";
            this.report1Btn.Size = new System.Drawing.Size(75, 23);
            this.report1Btn.TabIndex = 8;
            this.report1Btn.Text = "Звіт №1";
            this.report1Btn.UseVisualStyleBackColor = true;
            this.report1Btn.Click += new System.EventHandler(this.report1Btn_Click);
            // 
            // report2Btn
            // 
            this.report2Btn.Location = new System.Drawing.Point(33, 72);
            this.report2Btn.Name = "report2Btn";
            this.report2Btn.Size = new System.Drawing.Size(75, 23);
            this.report2Btn.TabIndex = 9;
            this.report2Btn.Text = "Звіт №2";
            this.report2Btn.UseVisualStyleBackColor = true;
            this.report2Btn.Click += new System.EventHandler(this.report2Btn_Click);
            // 
            // report3Btn
            // 
            this.report3Btn.Location = new System.Drawing.Point(33, 119);
            this.report3Btn.Name = "report3Btn";
            this.report3Btn.Size = new System.Drawing.Size(75, 23);
            this.report3Btn.TabIndex = 10;
            this.report3Btn.Text = "Звіт №3";
            this.report3Btn.UseVisualStyleBackColor = true;
            this.report3Btn.Click += new System.EventHandler(this.report3Btn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.coursesBtn);
            this.groupBox1.Controls.Add(this.applicationsBtn);
            this.groupBox1.Controls.Add(this.workersBtn);
            this.groupBox1.Controls.Add(this.storesBtn);
            this.groupBox1.Location = new System.Drawing.Point(24, 118);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(164, 215);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Робота з таблицями";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.report3Btn);
            this.groupBox2.Controls.Add(this.report2Btn);
            this.groupBox2.Controls.Add(this.report1Btn);
            this.groupBox2.Location = new System.Drawing.Point(223, 118);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(144, 215);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Звіти";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 355);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button storesBtn;
        private System.Windows.Forms.Button workersBtn;
        private System.Windows.Forms.Button applicationsBtn;
        private System.Windows.Forms.Button coursesBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button report1Btn;
        private System.Windows.Forms.Button report2Btn;
        private System.Windows.Forms.Button report3Btn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}

