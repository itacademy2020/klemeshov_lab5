﻿
namespace Klemeshov_lab5
{
    partial class FormCourses
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbCourses = new System.Windows.Forms.ComboBox();
            this.showCoursesBtn = new System.Windows.Forms.Button();
            this.updateCourseBtn = new System.Windows.Forms.Button();
            this.addCourse = new System.Windows.Forms.Button();
            this.endDateTextBox = new System.Windows.Forms.TextBox();
            this.startDateTextBox = new System.Windows.Forms.TextBox();
            this.numStudentsCount = new System.Windows.Forms.NumericUpDown();
            this.numModulesCount = new System.Windows.Forms.NumericUpDown();
            this.cmbCoachId = new System.Windows.Forms.ComboBox();
            this.courseNameTextBox = new System.Windows.Forms.TextBox();
            this.dgvCourses = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStudentsCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numModulesCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCourses)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label7);
            this.splitContainer1.Panel1.Controls.Add(this.label6);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.cmbCourses);
            this.splitContainer1.Panel1.Controls.Add(this.showCoursesBtn);
            this.splitContainer1.Panel1.Controls.Add(this.updateCourseBtn);
            this.splitContainer1.Panel1.Controls.Add(this.addCourse);
            this.splitContainer1.Panel1.Controls.Add(this.endDateTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.startDateTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.numStudentsCount);
            this.splitContainer1.Panel1.Controls.Add(this.numModulesCount);
            this.splitContainer1.Panel1.Controls.Add(this.cmbCoachId);
            this.splitContainer1.Panel1.Controls.Add(this.courseNameTextBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvCourses);
            this.splitContainer1.Size = new System.Drawing.Size(800, 450);
            this.splitContainer1.SplitterDistance = 266;
            this.splitContainer1.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(162, 243);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 30);
            this.label7.TabIndex = 16;
            this.label7.Text = "Виберіть id \r\nкурсу для зміни";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 258);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 15);
            this.label6.TabIndex = 15;
            this.label6.Text = "Дата закінчення ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 207);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(178, 15);
            this.label5.TabIndex = 14;
            this.label5.Text = "Дата початку (рік-місяць-день)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 149);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 15);
            this.label4.TabIndex = 13;
            this.label4.Text = "Кількість студентів";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 15);
            this.label3.TabIndex = 12;
            this.label3.Text = "Кількість модулів";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 15);
            this.label2.TabIndex = 11;
            this.label2.Text = "Id викладача";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 15);
            this.label1.TabIndex = 10;
            this.label1.Text = "Назва курса";
            // 
            // cmbCourses
            // 
            this.cmbCourses.FormattingEnabled = true;
            this.cmbCourses.Location = new System.Drawing.Point(162, 279);
            this.cmbCourses.Name = "cmbCourses";
            this.cmbCourses.Size = new System.Drawing.Size(94, 23);
            this.cmbCourses.TabIndex = 9;
            // 
            // showCoursesBtn
            // 
            this.showCoursesBtn.Location = new System.Drawing.Point(13, 365);
            this.showCoursesBtn.Name = "showCoursesBtn";
            this.showCoursesBtn.Size = new System.Drawing.Size(243, 73);
            this.showCoursesBtn.TabIndex = 8;
            this.showCoursesBtn.Text = "Показати дані";
            this.showCoursesBtn.UseVisualStyleBackColor = true;
            this.showCoursesBtn.Click += new System.EventHandler(this.showCoursesBtn_Click);
            // 
            // updateCourseBtn
            // 
            this.updateCourseBtn.Location = new System.Drawing.Point(162, 317);
            this.updateCourseBtn.Name = "updateCourseBtn";
            this.updateCourseBtn.Size = new System.Drawing.Size(94, 23);
            this.updateCourseBtn.TabIndex = 7;
            this.updateCourseBtn.Text = "Оновити";
            this.updateCourseBtn.UseVisualStyleBackColor = true;
            this.updateCourseBtn.Click += new System.EventHandler(this.updateCourseBtn_Click);
            // 
            // addCourse
            // 
            this.addCourse.Location = new System.Drawing.Point(11, 317);
            this.addCourse.Name = "addCourse";
            this.addCourse.Size = new System.Drawing.Size(122, 23);
            this.addCourse.TabIndex = 6;
            this.addCourse.Text = "Додати";
            this.addCourse.UseVisualStyleBackColor = true;
            this.addCourse.Click += new System.EventHandler(this.addCourse_Click);
            // 
            // endDateTextBox
            // 
            this.endDateTextBox.Location = new System.Drawing.Point(13, 279);
            this.endDateTextBox.Name = "endDateTextBox";
            this.endDateTextBox.Size = new System.Drawing.Size(120, 23);
            this.endDateTextBox.TabIndex = 5;
            // 
            // startDateTextBox
            // 
            this.startDateTextBox.Location = new System.Drawing.Point(13, 228);
            this.startDateTextBox.Name = "startDateTextBox";
            this.startDateTextBox.Size = new System.Drawing.Size(120, 23);
            this.startDateTextBox.TabIndex = 4;
            // 
            // numStudentsCount
            // 
            this.numStudentsCount.Location = new System.Drawing.Point(13, 169);
            this.numStudentsCount.Name = "numStudentsCount";
            this.numStudentsCount.Size = new System.Drawing.Size(120, 23);
            this.numStudentsCount.TabIndex = 3;
            // 
            // numModulesCount
            // 
            this.numModulesCount.Location = new System.Drawing.Point(13, 119);
            this.numModulesCount.Name = "numModulesCount";
            this.numModulesCount.Size = new System.Drawing.Size(120, 23);
            this.numModulesCount.TabIndex = 2;
            // 
            // cmbCoachId
            // 
            this.cmbCoachId.FormattingEnabled = true;
            this.cmbCoachId.Location = new System.Drawing.Point(12, 76);
            this.cmbCoachId.Name = "cmbCoachId";
            this.cmbCoachId.Size = new System.Drawing.Size(121, 23);
            this.cmbCoachId.TabIndex = 1;
            // 
            // courseNameTextBox
            // 
            this.courseNameTextBox.Location = new System.Drawing.Point(13, 28);
            this.courseNameTextBox.Name = "courseNameTextBox";
            this.courseNameTextBox.Size = new System.Drawing.Size(120, 23);
            this.courseNameTextBox.TabIndex = 0;
            // 
            // dgvCourses
            // 
            this.dgvCourses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCourses.Location = new System.Drawing.Point(4, 4);
            this.dgvCourses.Name = "dgvCourses";
            this.dgvCourses.RowTemplate.Height = 25;
            this.dgvCourses.Size = new System.Drawing.Size(526, 446);
            this.dgvCourses.TabIndex = 0;
            // 
            // FormCourses
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "FormCourses";
            this.Text = "FormCourses";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormCourses_FormClosed);
            this.Load += new System.EventHandler(this.FormCourses_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numStudentsCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numModulesCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCourses)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button showCoursesBtn;
        private System.Windows.Forms.Button updateCourseBtn;
        private System.Windows.Forms.Button addCourse;
        private System.Windows.Forms.TextBox endDateTextBox;
        private System.Windows.Forms.TextBox startDateTextBox;
        private System.Windows.Forms.NumericUpDown numStudentsCount;
        private System.Windows.Forms.NumericUpDown numModulesCount;
        private System.Windows.Forms.ComboBox cmbCoachId;
        private System.Windows.Forms.TextBox courseNameTextBox;
        private System.Windows.Forms.DataGridView dgvCourses;
        private System.Windows.Forms.ComboBox cmbCourses;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}