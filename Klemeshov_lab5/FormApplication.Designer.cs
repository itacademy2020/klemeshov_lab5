﻿
namespace Klemeshov_lab5
{
    partial class FormApplication
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.showApplications = new System.Windows.Forms.Button();
            this.updateApplication = new System.Windows.Forms.Button();
            this.addApplication = new System.Windows.Forms.Button();
            this.cmbApplications = new System.Windows.Forms.ComboBox();
            this.cmbCoursesApplicationsId = new System.Windows.Forms.ComboBox();
            this.cmbWorkersApplicationsId = new System.Windows.Forms.ComboBox();
            this.fillOutDateTextBox = new System.Windows.Forms.TextBox();
            this.dgvApplications = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvApplications)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.showApplications);
            this.splitContainer1.Panel1.Controls.Add(this.updateApplication);
            this.splitContainer1.Panel1.Controls.Add(this.addApplication);
            this.splitContainer1.Panel1.Controls.Add(this.cmbApplications);
            this.splitContainer1.Panel1.Controls.Add(this.cmbCoursesApplicationsId);
            this.splitContainer1.Panel1.Controls.Add(this.cmbWorkersApplicationsId);
            this.splitContainer1.Panel1.Controls.Add(this.fillOutDateTextBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvApplications);
            this.splitContainer1.Size = new System.Drawing.Size(800, 450);
            this.splitContainer1.SplitterDistance = 266;
            this.splitContainer1.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 284);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(192, 15);
            this.label4.TabIndex = 10;
            this.label4.Text = "Виберіть Id заявки для оновлення";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(235, 15);
            this.label3.TabIndex = 9;
            this.label3.Text = "Дата заповнення заявки (рік-місяць-дата)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 15);
            this.label2.TabIndex = 8;
            this.label2.Text = "Id Курса";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 15);
            this.label1.TabIndex = 7;
            this.label1.Text = "Id Працівника";
            // 
            // showApplications
            // 
            this.showApplications.Location = new System.Drawing.Point(32, 377);
            this.showApplications.Name = "showApplications";
            this.showApplications.Size = new System.Drawing.Size(151, 61);
            this.showApplications.TabIndex = 6;
            this.showApplications.Text = "Показати дані";
            this.showApplications.UseVisualStyleBackColor = true;
            this.showApplications.Click += new System.EventHandler(this.showApplications_Click);
            // 
            // updateApplication
            // 
            this.updateApplication.Location = new System.Drawing.Point(32, 334);
            this.updateApplication.Name = "updateApplication";
            this.updateApplication.Size = new System.Drawing.Size(121, 23);
            this.updateApplication.TabIndex = 5;
            this.updateApplication.Text = "Оновити";
            this.updateApplication.UseVisualStyleBackColor = true;
            this.updateApplication.Click += new System.EventHandler(this.updateApplication_Click);
            // 
            // addApplication
            // 
            this.addApplication.Location = new System.Drawing.Point(32, 200);
            this.addApplication.Name = "addApplication";
            this.addApplication.Size = new System.Drawing.Size(121, 23);
            this.addApplication.TabIndex = 4;
            this.addApplication.Text = "Додати";
            this.addApplication.UseVisualStyleBackColor = true;
            this.addApplication.Click += new System.EventHandler(this.addApplication_Click);
            // 
            // cmbApplications
            // 
            this.cmbApplications.FormattingEnabled = true;
            this.cmbApplications.Location = new System.Drawing.Point(32, 305);
            this.cmbApplications.Name = "cmbApplications";
            this.cmbApplications.Size = new System.Drawing.Size(121, 23);
            this.cmbApplications.TabIndex = 3;
            // 
            // cmbCoursesApplicationsId
            // 
            this.cmbCoursesApplicationsId.FormattingEnabled = true;
            this.cmbCoursesApplicationsId.Location = new System.Drawing.Point(32, 105);
            this.cmbCoursesApplicationsId.Name = "cmbCoursesApplicationsId";
            this.cmbCoursesApplicationsId.Size = new System.Drawing.Size(121, 23);
            this.cmbCoursesApplicationsId.TabIndex = 2;
            // 
            // cmbWorkersApplicationsId
            // 
            this.cmbWorkersApplicationsId.FormattingEnabled = true;
            this.cmbWorkersApplicationsId.Location = new System.Drawing.Point(32, 49);
            this.cmbWorkersApplicationsId.Name = "cmbWorkersApplicationsId";
            this.cmbWorkersApplicationsId.Size = new System.Drawing.Size(121, 23);
            this.cmbWorkersApplicationsId.TabIndex = 1;
            // 
            // fillOutDateTextBox
            // 
            this.fillOutDateTextBox.Location = new System.Drawing.Point(32, 159);
            this.fillOutDateTextBox.Name = "fillOutDateTextBox";
            this.fillOutDateTextBox.Size = new System.Drawing.Size(121, 23);
            this.fillOutDateTextBox.TabIndex = 0;
            // 
            // dgvApplications
            // 
            this.dgvApplications.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvApplications.Location = new System.Drawing.Point(4, 4);
            this.dgvApplications.Name = "dgvApplications";
            this.dgvApplications.RowTemplate.Height = 25;
            this.dgvApplications.Size = new System.Drawing.Size(526, 446);
            this.dgvApplications.TabIndex = 0;
            // 
            // FormApplication
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "FormApplication";
            this.Text = "FormApplication";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormApplication_FormClosed);
            this.Load += new System.EventHandler(this.FormApplication_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvApplications)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ComboBox cmbWorkerId;
        private System.Windows.Forms.TextBox fillOutDateTextBox;
        private System.Windows.Forms.DataGridView dgvApplications;
        private System.Windows.Forms.ComboBox cmbCoursesApplicationsId;
        private System.Windows.Forms.ComboBox cmbWorkersApplicationsId;
        private System.Windows.Forms.ComboBox cmbApplications;
        private System.Windows.Forms.Button showApplications;
        private System.Windows.Forms.Button updateApplication;
        private System.Windows.Forms.Button addApplication;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
    }
}