﻿
namespace Klemeshov_lab5
{
    partial class FormStores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.storeTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.addStoreBtn = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label3 = new System.Windows.Forms.Label();
            this.updateStoresBtn = new System.Windows.Forms.Button();
            this.cmbStores = new System.Windows.Forms.ComboBox();
            this.showStoresBtn = new System.Windows.Forms.Button();
            this.comNetworkTextBox = new System.Windows.Forms.TextBox();
            this.dgvStores = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStores)).BeginInit();
            this.SuspendLayout();
            // 
            // storeTextBox
            // 
            this.storeTextBox.Location = new System.Drawing.Point(16, 32);
            this.storeTextBox.Name = "storeTextBox";
            this.storeTextBox.Size = new System.Drawing.Size(132, 23);
            this.storeTextBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Назва магазину";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Назва торгової мережі";
            // 
            // addStoreBtn
            // 
            this.addStoreBtn.Location = new System.Drawing.Point(16, 136);
            this.addStoreBtn.Name = "addStoreBtn";
            this.addStoreBtn.Size = new System.Drawing.Size(132, 23);
            this.addStoreBtn.TabIndex = 4;
            this.addStoreBtn.Text = "Додати";
            this.addStoreBtn.UseVisualStyleBackColor = true;
            this.addStoreBtn.Click += new System.EventHandler(this.addStoreBtn_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(12, 12);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.updateStoresBtn);
            this.splitContainer1.Panel1.Controls.Add(this.cmbStores);
            this.splitContainer1.Panel1.Controls.Add(this.showStoresBtn);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.addStoreBtn);
            this.splitContainer1.Panel1.Controls.Add(this.storeTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.comNetworkTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvStores);
            this.splitContainer1.Size = new System.Drawing.Size(776, 426);
            this.splitContainer1.SplitterDistance = 258;
            this.splitContainer1.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 222);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 15);
            this.label3.TabIndex = 8;
            this.label3.Text = "id магазину";
            // 
            // updateStoresBtn
            // 
            this.updateStoresBtn.Location = new System.Drawing.Point(16, 278);
            this.updateStoresBtn.Name = "updateStoresBtn";
            this.updateStoresBtn.Size = new System.Drawing.Size(121, 23);
            this.updateStoresBtn.TabIndex = 7;
            this.updateStoresBtn.Text = "Оновити";
            this.updateStoresBtn.UseVisualStyleBackColor = true;
            this.updateStoresBtn.Click += new System.EventHandler(this.updateStoresBtn_Click);
            // 
            // cmbStores
            // 
            this.cmbStores.FormattingEnabled = true;
            this.cmbStores.Location = new System.Drawing.Point(16, 240);
            this.cmbStores.Name = "cmbStores";
            this.cmbStores.Size = new System.Drawing.Size(121, 23);
            this.cmbStores.TabIndex = 6;
            // 
            // showStoresBtn
            // 
            this.showStoresBtn.Location = new System.Drawing.Point(18, 350);
            this.showStoresBtn.Name = "showStoresBtn";
            this.showStoresBtn.Size = new System.Drawing.Size(216, 55);
            this.showStoresBtn.TabIndex = 5;
            this.showStoresBtn.Text = "Показати дані";
            this.showStoresBtn.UseVisualStyleBackColor = true;
            this.showStoresBtn.Click += new System.EventHandler(this.showStoresBtn_Click);
            // 
            // comNetworkTextBox
            // 
            this.comNetworkTextBox.Location = new System.Drawing.Point(16, 95);
            this.comNetworkTextBox.Name = "comNetworkTextBox";
            this.comNetworkTextBox.Size = new System.Drawing.Size(132, 23);
            this.comNetworkTextBox.TabIndex = 3;
            // 
            // dgvStores
            // 
            this.dgvStores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStores.Location = new System.Drawing.Point(3, 3);
            this.dgvStores.Name = "dgvStores";
            this.dgvStores.RowTemplate.Height = 25;
            this.dgvStores.Size = new System.Drawing.Size(508, 420);
            this.dgvStores.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 197);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(207, 15);
            this.label4.TabIndex = 9;
            this.label4.Text = "Виберіть id магазину для оновлення";
            // 
            // FormStores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "FormStores";
            this.Text = "FormStores";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormStores_FormClosed);
            this.Load += new System.EventHandler(this.FormStores_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvStores)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox storeTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button addStoreBtn;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button showStoresBtn;
        private System.Windows.Forms.TextBox comNetworkTextBox;
        private System.Windows.Forms.DataGridView dgvStores;
        private System.Windows.Forms.ComboBox cmbStores;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button updateStoresBtn;
        private System.Windows.Forms.Label label4;
    }
}