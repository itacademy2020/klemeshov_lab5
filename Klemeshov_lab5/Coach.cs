﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Klemeshov_lab5
{
    public class Coach
    {
        public Coach(int id, String fullName)
        {
            this.id = id;
            this.fullName = fullName;
        }

        public int id { get; set; }
        public String fullName { get; set; }
    }
}
