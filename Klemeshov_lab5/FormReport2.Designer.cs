﻿
namespace Klemeshov_lab5
{
    partial class FormReport2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbCoachReport2 = new System.Windows.Forms.ComboBox();
            this.ShowReport2Btn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvReport2 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport2)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.cmbCoachReport2);
            this.splitContainer1.Panel1.Controls.Add(this.ShowReport2Btn);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvReport2);
            this.splitContainer1.Size = new System.Drawing.Size(800, 450);
            this.splitContainer1.SplitterDistance = 266;
            this.splitContainer1.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 216);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "Виберіть викладача";
            // 
            // cmbCoachReport2
            // 
            this.cmbCoachReport2.FormattingEnabled = true;
            this.cmbCoachReport2.Location = new System.Drawing.Point(12, 237);
            this.cmbCoachReport2.Name = "cmbCoachReport2";
            this.cmbCoachReport2.Size = new System.Drawing.Size(121, 23);
            this.cmbCoachReport2.TabIndex = 4;
            // 
            // ShowReport2Btn
            // 
            this.ShowReport2Btn.Location = new System.Drawing.Point(12, 354);
            this.ShowReport2Btn.Name = "ShowReport2Btn";
            this.ShowReport2Btn.Size = new System.Drawing.Size(241, 84);
            this.ShowReport2Btn.TabIndex = 2;
            this.ShowReport2Btn.Text = "Показати звіт";
            this.ShowReport2Btn.UseVisualStyleBackColor = true;
            this.ShowReport2Btn.Click += new System.EventHandler(this.ShowReport2Btn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(12, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(209, 105);
            this.label2.TabIndex = 1;
            this.label2.Text = "Вивести список магазинів \r\n(вказавши мережу до  якої\r\n належить магазин) які \r\nза" +
    "реєструвалися на тренінг \r\nвикладача.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(88, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "Звіт №2";
            // 
            // dgvReport2
            // 
            this.dgvReport2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReport2.Location = new System.Drawing.Point(4, 4);
            this.dgvReport2.Name = "dgvReport2";
            this.dgvReport2.RowTemplate.Height = 25;
            this.dgvReport2.Size = new System.Drawing.Size(526, 446);
            this.dgvReport2.TabIndex = 0;
            // 
            // FormReport2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "FormReport2";
            this.Text = "FormReport2";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormReport2_FormClosed);
            this.Load += new System.EventHandler(this.FormReport2_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvReport2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbCoachReport2;
        private System.Windows.Forms.Button ShowReport2Btn;
    }
}