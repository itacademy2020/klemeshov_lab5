﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Klemeshov_lab5
{
    public partial class FormReport1 : Form
    {
        String sqlConnectionString = "Server=tcp:itacademy.database.windows.net,1433;Database=Klemeshov;User ID = Klemeshov;Password=Iszv12380;Trusted_Connection=False;Encrypt=True;";
        public FormReport1()
        {
            InitializeComponent();
        }

        private void FormReport1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form ifrm = Application.OpenForms[0];
            ifrm.StartPosition = FormStartPosition.Manual;
            ifrm.Left = this.Left;
            ifrm.Top = this.Top;
            ifrm.Show();
        }

        private void FormReport1_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlConnectionString);
            con.Open();


            SqlCommand comm = new SqlCommand("SELECT [id], [full_name] FROM [dbo].[coaches]", con);

            SqlDataReader dr = comm.ExecuteReader();

            cmbReport1.Items.Clear();
            while (dr.Read())
            {
                cmbReport1.Items.Add(new Coach((int)dr[0], dr[1].ToString()));
                cmbReport1.DisplayMember = "fullName";
            }

            con.Close();

        }

        private void showReport1Btn_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlConnectionString);
            try { 
            con.Open();
            
                Coach currentItem = (Coach)cmbReport1.SelectedItem;

                if(currentItem != null) 
                {
                    SqlDataAdapter ad = new SqlDataAdapter("SELECT DISTINCT [courses].*" +
    " FROM[courses]" +
    " INNER JOIN[coaches]" +
    " ON courses.coach_id = coaches.id" +
    " INNER JOIN[applications]" +
    " ON[applications].[course_id] = [courses].[id]" +
    " INNER JOIN[workers]" +
    " ON[workers].[id] = [applications].[worker_id]" +
    " INNER JOIN[testings]" +
    " ON[testings].[worker_id] = [workers].[id]" +
    " INNER JOIN[tests]" +
    " ON[testings].[test_id] = [tests].[id]" +
    " WHERE[coaches].[full_name] = '" + currentItem.fullName + "' AND [tests].[id] IS NOT NULL; ", con);
                    DataSet ds = new DataSet();
                    ad.Fill(ds, "Report1");
                    dgvReport1.DataSource = ds.Tables["Report1"]; ;
                    dgvReport1.Refresh();
                } else
                {
                    MessageBox.Show("Виберіть тренера!");
                }

            
            } 
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }

            con.Close();
        }
    }
}
