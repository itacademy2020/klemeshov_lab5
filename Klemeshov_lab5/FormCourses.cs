﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Klemeshov_lab5
{
    public partial class FormCourses : Form
    {
        String sqlConnectionString = "Server=tcp:itacademy.database.windows.net,1433;Database=Klemeshov;User ID = Klemeshov;Password=Iszv12380;Trusted_Connection=False;Encrypt=True;";
        public FormCourses()
        {
            InitializeComponent();
        }

        private void FormCourses_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form ifrm = Application.OpenForms[0];
            ifrm.StartPosition = FormStartPosition.Manual;
            ifrm.Left = this.Left;
            ifrm.Top = this.Top;
            ifrm.Show();
        }

        private void showCoursesBtn_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlConnectionString);
            con.Open();

            SqlDataAdapter ad = new SqlDataAdapter($"SELECT * FROM [dbo].[courses]", con);
            DataSet ds = new DataSet();
            ad.Fill(ds, "Courses");
            dgvCourses.DataSource = ds.Tables["Courses"];
            dgvCourses.Refresh();
            con.Close();
        }

        private void addCourse_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlConnectionString);
            try
            {

                con.Open();
                if (!String.IsNullOrWhiteSpace(courseNameTextBox.Text) &&
                    cmbCoachId.SelectedItem != null &&
                    numModulesCount.ToString() != "" &&
                    numStudentsCount.Value.ToString() != "" &&
                    !String.IsNullOrWhiteSpace(startDateTextBox.Text) &&
                    !String.IsNullOrWhiteSpace(endDateTextBox.Text))
                {
                    SqlCommand command = new SqlCommand($"INSERT INTO [dbo].[courses] ([course_name], [coach_id], [modules_count], [students_count], [start_date], [end_date]) VALUES ('{courseNameTextBox.Text.ToString()}', {cmbCoachId.SelectedItem.ToString()}, '{numModulesCount.Value.ToString()}', {numStudentsCount.Value.ToString()}, '{startDateTextBox.Text.ToString()}', '{endDateTextBox.Text.ToString()}')", con);
                    command.ExecuteNonQuery();
                    MessageBox.Show("Курс додано успішно!");

                    command = new SqlCommand("SELECT [id] FROM [dbo].[courses]", con);

                    SqlDataReader dr = command.ExecuteReader();

                    cmbCourses.Items.Clear();
                    while (dr.Read())
                    {
                        cmbCourses.Items.Add(dr[0].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Заповніть всі поля і перевірте формат дати!");
                }
            }
            catch (Exception err)
            {
                MessageBox.Show($"Помилка при додаванні в таблицю courses! ({err.Message})");
            }
            finally
            {
                con.Close();
                courseNameTextBox.Clear();
                cmbCoachId.SelectedIndex = -1;
                numModulesCount.Value = 0;
                numStudentsCount.Value = 0;
                startDateTextBox.Clear();
                endDateTextBox.Clear();
            }
        }

        private void FormCourses_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlConnectionString);
            con.Open();


            SqlCommand comm = new SqlCommand("SELECT [id] FROM [dbo].[coaches]", con);

            SqlDataReader dr = comm.ExecuteReader();

            cmbCoachId.Items.Clear();
            while (dr.Read())
            {
                cmbCoachId.Items.Add(dr[0].ToString());
            }

            dr.Close();

            comm = new SqlCommand("SELECT [id] FROM [dbo].[courses]", con);

            dr = comm.ExecuteReader();

            cmbCourses.Items.Clear();
            while (dr.Read())
            {
                cmbCourses.Items.Add(dr[0].ToString());
            }
        }

        private void updateCourseBtn_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlConnectionString);
            try
            {

                con.Open();
                if (!String.IsNullOrWhiteSpace(courseNameTextBox.Text) &&
                    cmbCoachId.SelectedItem != null &&
                    numModulesCount.Value.ToString() != "" &&
                    numStudentsCount.Value.ToString() != "" &&
                    !String.IsNullOrWhiteSpace(startDateTextBox.Text) &&
                    !String.IsNullOrWhiteSpace(endDateTextBox.Text) &&
                    cmbCourses.SelectedItem != null)
                {
                    SqlCommand command = new SqlCommand($"UPDATE [dbo].[courses] SET [course_name] = '{courseNameTextBox.Text.ToString()}', [coach_id] = {cmbCoachId.SelectedItem.ToString()}, [modules_count] = {numModulesCount.Value.ToString()}, [students_count] = {numStudentsCount.Value.ToString()}, [start_date] = '{startDateTextBox.Text.ToString()}', [end_date] = '{endDateTextBox.Text.ToString()}' WHERE [id] = {cmbCourses.SelectedItem.ToString()}", con);
                    command.ExecuteNonQuery();
                    MessageBox.Show("Поле оновлено успішно!");
                }
                else
                {
                    MessageBox.Show("Заповніть всі поля і перевірте формат дати!");
                }
            }
            catch (Exception err)
            {
                MessageBox.Show($"Помилка при оновленні поля таблиці courses! ({err.Message})");
            }
            finally
            {
                con.Close();
                courseNameTextBox.Clear();
                cmbCoachId.SelectedIndex = -1;
                numModulesCount.Value = 0;
                numStudentsCount.Value = 0;
                startDateTextBox.Clear();
                endDateTextBox.Clear();
            }
        }
    }  
}


