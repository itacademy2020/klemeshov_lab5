﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Klemeshov_lab5
{
    public partial class FormWorkers : Form
    {
        String sqlConnectionString = "Server=tcp:itacademy.database.windows.net,1433;Database=Klemeshov;User ID = Klemeshov;Password=Iszv12380;Trusted_Connection=False;Encrypt=True;";
        public FormWorkers()
        {
            InitializeComponent();
        }

        private void showWorkersBtn_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlConnectionString);
            con.Open();

            SqlDataAdapter ad = new SqlDataAdapter($"SELECT * FROM workers", con);
            DataSet ds = new DataSet();
            ad.Fill(ds, "Workers");
            dgvWorkers.DataSource = ds.Tables["Workers"];
            dgvWorkers.Refresh();
            con.Close();
        }

        private void FormWorkers_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlConnectionString);
            con.Open();


            SqlCommand comm = new SqlCommand("SELECT [id] FROM [dbo].[stores]", con);

            SqlDataReader dr = comm.ExecuteReader();

            cmbWorkersStoreId.Items.Clear();
            while (dr.Read())
            {
                cmbWorkersStoreId.Items.Add(dr[0].ToString());
            }

            dr.Close();

            comm = new SqlCommand("SELECT [id] FROM [dbo].[workers]", con);

             dr = comm.ExecuteReader();

            cmbWorkers.Items.Clear();
            while (dr.Read())
            {
                cmbWorkers.Items.Add(dr[0].ToString());
            }

            con.Close();
        }

        private void FormWorkers_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form ifrm = Application.OpenForms[0];
            ifrm.StartPosition = FormStartPosition.Manual;
            ifrm.Left = this.Left;
            ifrm.Top = this.Top;
            ifrm.Show();
        }

        private void addWorkerBtn_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlConnectionString);
            try
            {

                con.Open();
                if (cmbWorkersStoreId.SelectedItem != null &&
                    !String.IsNullOrWhiteSpace(fullNameTextBox.Text))
                {
                    SqlCommand command = new SqlCommand($"INSERT INTO [dbo].[workers] ([full_name], [store_id]) VALUES ('{fullNameTextBox.Text.ToString()}', {cmbWorkersStoreId.SelectedItem.ToString()})", con);
                    command.ExecuteNonQuery();
                    MessageBox.Show("Працівника додано успішно!");

                    command = new SqlCommand("SELECT [id] FROM [dbo].[workers]", con);

                    SqlDataReader dr = command.ExecuteReader();

                    cmbWorkers.Items.Clear();
                    while (dr.Read())
                    {
                        cmbWorkers.Items.Add(dr[0].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Заповніть всі поля!");
                }
            }
            catch (Exception err)
            {
                MessageBox.Show($"Помилка при додаванні в таблицю workers! ({err.Message})");
            }
            finally
            {
                con.Close();
                fullNameTextBox.Clear();
                cmbWorkersStoreId.SelectedIndex = -1;
            }
        }

        private void updateWorkerBtn_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlConnectionString);
            try
            {

                con.Open();
                if (cmbWorkersStoreId.SelectedItem != null &&
                   !String.IsNullOrWhiteSpace(fullNameTextBox.Text) && 
                    cmbWorkers.SelectedItem != null)
                {
                    SqlCommand command = new SqlCommand($"UPDATE [dbo].[workers] SET [full_name] = '{fullNameTextBox.Text.ToString()}', [store_id] = {cmbWorkersStoreId.SelectedItem.ToString()} WHERE [id] = {cmbWorkers.SelectedItem.ToString()}", con);
                    command.ExecuteNonQuery();
                    MessageBox.Show("Поле оновлено успішно!");
                }
                else
                {
                    MessageBox.Show("Заповніть всі поля!");
                }
            }
            catch (Exception err)
            {
                MessageBox.Show($"Помилка при оновленні поля таблиці workers! ({err.Message})");
            }
            finally
            {
                con.Close();
                fullNameTextBox.Clear();
                cmbWorkersStoreId.SelectedIndex = -1;
            }
        }
    }
}
