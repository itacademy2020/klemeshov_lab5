﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Klemeshov_lab5
{
    public partial class FormReport3 : Form
    {
        String sqlConnectionString = "Server=tcp:itacademy.database.windows.net,1433;Database=Klemeshov;User ID = Klemeshov;Password=Iszv12380;Trusted_Connection=False;Encrypt=True;";
        public FormReport3()
        {
            InitializeComponent();
        }

        private void FormReport3_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form ifrm = Application.OpenForms[0];
            ifrm.StartPosition = FormStartPosition.Manual;
            ifrm.Left = this.Left;
            ifrm.Top = this.Top;
            ifrm.Show();
        }

        private void FormReport3_Load(object sender, EventArgs e)
        {

        }

        private void showReport3Btn_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlConnectionString);
            con.Open();

            SqlDataAdapter ad = new SqlDataAdapter("SELECT [workers].[full_name], [stores].[store], [themes].[theme], [courses].[course_name]" +
"FROM[workers]" +
"INNER JOIN[stores]" +
"ON[workers].[store_id] = [stores].[id]" +
"INNER JOIN[testings]" +
"ON[testings].[worker_id] = [workers].[id]" +
"INNER JOIN[tests]" +
"ON[tests].[id] = [testings].[test_id]" +
"INNER JOIN[themes]" +
"ON[tests].[id] = [themes].[id]" +
"INNER JOIN[applications]" +
"ON[applications].[worker_id] = [workers].[id]" +
"INNER JOIN[courses]" +
"ON[courses].[id] = [applications].[course_id]" +
"INNER JOIN[ratings]" +
"ON[ratings].[worker_id] = [workers].[id]" +
"WHERE[ratings].[all_testings_score] > " + numUpDown.Value + "; ", con);
            DataSet ds = new DataSet();
            ad.Fill(ds, "Report3");
            dgvReport3.DataSource = ds.Tables["Report3"];
            dgvReport3.Refresh();
            con.Close();
        }


    }
}
