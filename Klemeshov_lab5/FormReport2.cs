﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Klemeshov_lab5
{
    public partial class FormReport2 : Form
    {
        String sqlConnectionString = "Server=tcp:itacademy.database.windows.net,1433;Database=Klemeshov;User ID = Klemeshov;Password=Iszv12380;Trusted_Connection=False;Encrypt=True;";
        public FormReport2()
        {
            InitializeComponent();
        }

        private void FormReport2_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form ifrm = Application.OpenForms[0];
            ifrm.StartPosition = FormStartPosition.Manual;
            ifrm.Left = this.Left;
            ifrm.Top = this.Top;
            ifrm.Show();
        }

        private void FormReport2_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlConnectionString);
            con.Open();


            SqlCommand comm = new SqlCommand("SELECT [full_name] FROM [dbo].[coaches]", con);

            SqlDataReader dr = comm.ExecuteReader();

            cmbCoachReport2.Items.Clear();
            while (dr.Read())
            {
                cmbCoachReport2.Items.Add(dr[0].ToString());
            }

            con.Close();
        }

        private void ShowReport2Btn_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlConnectionString);
            con.Open();

            Object currentItem = cmbCoachReport2.SelectedItem;

            if (currentItem != null)
            {

                SqlDataAdapter ad = new SqlDataAdapter("SELECT DISTINCT[stores].[store], [stores].[commercial_network]" +
    "FROM[workers] INNER JOIN[stores]" +
    "ON[workers].[store_id] = [stores].[id]" +
    "INNER JOIN[applications]" +
    "ON[applications].[worker_id] = [workers].[id]" +
    "INNER JOIN[courses]" +
    "ON[courses].[id] = [applications].[course_id]" +
    "INNER JOIN[coaches]" +
    "ON[coaches].[id] = [courses].[coach_id]" +
    "WHERE coaches.full_name = '" + currentItem.ToString() + "'; ", con);
                DataSet ds = new DataSet();
                ad.Fill(ds, "Report2");
                dgvReport2.DataSource = ds.Tables["Report2"];
                dgvReport2.Refresh();
            }
            else
            {
                MessageBox.Show("Виберіть вчителя!");
                con.Close();
            }
        }
    }
}
