﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Klemeshov_lab5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void storesBtn_Click(object sender, EventArgs e)
        {
            FormStores FormStores = new FormStores();
            FormStores.Show();
            this.Hide();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        private void workersBtn_Click(object sender, EventArgs e)
        {
            FormWorkers FormWorkers = new FormWorkers();
            FormWorkers.Show();
            this.Hide();
        }

        private void applicationsBtn_Click(object sender, EventArgs e)
        {
            FormApplication FormApplication = new FormApplication();
            FormApplication.Show();
            this.Hide();
        }

        private void coursesBtn_Click(object sender, EventArgs e)
        {
            FormCourses FormCourses = new FormCourses();
            FormCourses.Show();
            this.Hide();
        }

        private void report1Btn_Click(object sender, EventArgs e)
        {
            FormReport1 FormReport1 = new FormReport1();
            FormReport1.Show();
            this.Hide();
        }

        private void report2Btn_Click(object sender, EventArgs e)
        {
            FormReport2 FormReport2 = new FormReport2();
            FormReport2.Show();
            this.Hide();
        }

        private void report3Btn_Click(object sender, EventArgs e)
        {
            FormReport3 FormReport3 = new FormReport3();
            FormReport3.Show();
            this.Hide();
        }
    }
}
