﻿
namespace Klemeshov_lab5
{
    partial class FormWorkers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.cmbWorkers = new System.Windows.Forms.ComboBox();
            this.showWorkersBtn = new System.Windows.Forms.Button();
            this.updateWorkerBtn = new System.Windows.Forms.Button();
            this.addWorkerBtn = new System.Windows.Forms.Button();
            this.cmbWorkersStoreId = new System.Windows.Forms.ComboBox();
            this.fullNameTextBox = new System.Windows.Forms.TextBox();
            this.dgvWorkers = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWorkers)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.cmbWorkers);
            this.splitContainer1.Panel1.Controls.Add(this.showWorkersBtn);
            this.splitContainer1.Panel1.Controls.Add(this.updateWorkerBtn);
            this.splitContainer1.Panel1.Controls.Add(this.addWorkerBtn);
            this.splitContainer1.Panel1.Controls.Add(this.cmbWorkersStoreId);
            this.splitContainer1.Panel1.Controls.Add(this.fullNameTextBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvWorkers);
            this.splitContainer1.Size = new System.Drawing.Size(800, 450);
            this.splitContainer1.SplitterDistance = 265;
            this.splitContainer1.TabIndex = 0;
            // 
            // cmbWorkers
            // 
            this.cmbWorkers.FormattingEnabled = true;
            this.cmbWorkers.Location = new System.Drawing.Point(39, 265);
            this.cmbWorkers.Name = "cmbWorkers";
            this.cmbWorkers.Size = new System.Drawing.Size(121, 23);
            this.cmbWorkers.TabIndex = 5;
            // 
            // showWorkersBtn
            // 
            this.showWorkersBtn.Location = new System.Drawing.Point(39, 352);
            this.showWorkersBtn.Name = "showWorkersBtn";
            this.showWorkersBtn.Size = new System.Drawing.Size(176, 73);
            this.showWorkersBtn.TabIndex = 4;
            this.showWorkersBtn.Text = "Показати дані";
            this.showWorkersBtn.UseVisualStyleBackColor = true;
            this.showWorkersBtn.Click += new System.EventHandler(this.showWorkersBtn_Click);
            // 
            // updateWorkerBtn
            // 
            this.updateWorkerBtn.Location = new System.Drawing.Point(39, 294);
            this.updateWorkerBtn.Name = "updateWorkerBtn";
            this.updateWorkerBtn.Size = new System.Drawing.Size(121, 23);
            this.updateWorkerBtn.TabIndex = 3;
            this.updateWorkerBtn.Text = "Оновити";
            this.updateWorkerBtn.UseVisualStyleBackColor = true;
            this.updateWorkerBtn.Click += new System.EventHandler(this.updateWorkerBtn_Click);
            // 
            // addWorkerBtn
            // 
            this.addWorkerBtn.Location = new System.Drawing.Point(39, 149);
            this.addWorkerBtn.Name = "addWorkerBtn";
            this.addWorkerBtn.Size = new System.Drawing.Size(121, 23);
            this.addWorkerBtn.TabIndex = 2;
            this.addWorkerBtn.Text = "Додати";
            this.addWorkerBtn.UseVisualStyleBackColor = true;
            this.addWorkerBtn.Click += new System.EventHandler(this.addWorkerBtn_Click);
            // 
            // cmbWorkersStoreId
            // 
            this.cmbWorkersStoreId.FormattingEnabled = true;
            this.cmbWorkersStoreId.Location = new System.Drawing.Point(39, 120);
            this.cmbWorkersStoreId.Name = "cmbWorkersStoreId";
            this.cmbWorkersStoreId.Size = new System.Drawing.Size(121, 23);
            this.cmbWorkersStoreId.TabIndex = 1;
            // 
            // fullNameTextBox
            // 
            this.fullNameTextBox.Location = new System.Drawing.Point(39, 69);
            this.fullNameTextBox.Name = "fullNameTextBox";
            this.fullNameTextBox.Size = new System.Drawing.Size(121, 23);
            this.fullNameTextBox.TabIndex = 0;
            // 
            // dgvWorkers
            // 
            this.dgvWorkers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWorkers.Location = new System.Drawing.Point(3, 3);
            this.dgvWorkers.Name = "dgvWorkers";
            this.dgvWorkers.RowTemplate.Height = 25;
            this.dgvWorkers.Size = new System.Drawing.Size(525, 444);
            this.dgvWorkers.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 15);
            this.label1.TabIndex = 6;
            this.label1.Text = "Повне ім\'я";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 15);
            this.label2.TabIndex = 7;
            this.label2.Text = "Id магазина";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 244);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(195, 15);
            this.label3.TabIndex = 8;
            this.label3.Text = "Виберіть id працівника для заміни";
            // 
            // FormWorkers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "FormWorkers";
            this.Text = "FormWorkers";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormWorkers_FormClosed);
            this.Load += new System.EventHandler(this.FormWorkers_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvWorkers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgvWorkers;
        private System.Windows.Forms.TextBox fullNameTextBox;
        private System.Windows.Forms.Button addWorkers;
        private System.Windows.Forms.ComboBox cmbWorkersStoreId;
        private System.Windows.Forms.Button updateWorkerBtn;
        private System.Windows.Forms.Button addWorkerBtn;
        private System.Windows.Forms.Button showWorkersBtn;
        private System.Windows.Forms.ComboBox cmbWorkerId;
        private System.Windows.Forms.ComboBox cmbWorkers;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}