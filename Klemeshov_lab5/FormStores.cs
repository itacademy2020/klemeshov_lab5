﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Klemeshov_lab5
{
    public partial class FormStores : Form
    {
        String sqlConnectionString = "Server=tcp:itacademy.database.windows.net,1433;Database=Klemeshov;User ID = Klemeshov;Password=Iszv12380;Trusted_Connection=False;Encrypt=True;";
        public FormStores()
        {
            InitializeComponent();
        }

        private void FormStores_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form ifrm = Application.OpenForms[0];
            ifrm.StartPosition = FormStartPosition.Manual; 
            ifrm.Left = this.Left;
            ifrm.Top = this.Top;
            ifrm.Show();
        }

        private void FormStores_Load(object sender, EventArgs e)
        {
            
            SqlConnection con = new SqlConnection(sqlConnectionString);
            con.Open();

            SqlCommand comm = new SqlCommand("SELECT [id] FROM [dbo].[stores]", con);
            
            SqlDataReader dr = comm.ExecuteReader();

            cmbStores.Items.Clear();
            while (dr.Read())
            {
                cmbStores.Items.Add(dr[0].ToString());
            }

            con.Close();
        }

        private void addStoreBtn_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlConnectionString);
            try
            {
                con.Open();
                if (!String.IsNullOrWhiteSpace(storeTextBox.Text) &&
                    !String.IsNullOrWhiteSpace(comNetworkTextBox.Text))
                {
                    SqlCommand command = new SqlCommand($"INSERT INTO [dbo].[stores] ([store],[commercial_network]) VALUES ('{storeTextBox.Text.ToString()}', '{comNetworkTextBox.Text.ToString()}')", con);
                    command.ExecuteNonQuery();
                    MessageBox.Show("Магазин додано успішно!");

                    command = new SqlCommand("SELECT [id] FROM [dbo].[stores]", con);

                    SqlDataReader dr = command.ExecuteReader();

                    cmbStores.Items.Clear();
                    while (dr.Read())
                    {
                        cmbStores.Items.Add(dr[0].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Заповінть всі поля!");
                }
            }
            catch (Exception err)
            {
                MessageBox.Show($"Помилка при додаванні в таблицю stores! {err.Message}");
            }
            finally { 
                con.Close();
                storeTextBox.Clear();
                comNetworkTextBox.Clear();
            }

        }

  

        private void showStoresBtn_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlConnectionString);
            con.Open();

            SqlDataAdapter ad = new SqlDataAdapter($"SELECT * FROM stores", con);
            DataSet ds = new DataSet();
            ad.Fill(ds, "Stores");
            dgvStores.DataSource = ds.Tables["Stores"];
            dgvStores.Refresh();
            con.Close();
        }

        private void updateStoresBtn_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlConnectionString);
            try
            {

                con.Open();
                if (!String.IsNullOrWhiteSpace(storeTextBox.Text) &&
                    !String.IsNullOrWhiteSpace(comNetworkTextBox.Text) &&
                    cmbStores.SelectedItem != null)
                {
                    SqlCommand command = new SqlCommand($"UPDATE [dbo].[stores] SET [store] = '{storeTextBox.Text.ToString()}' ,[commercial_network] = '{comNetworkTextBox.Text.ToString()}' WHERE [id] = {cmbStores.SelectedItem.ToString()}", con);
                    command.ExecuteNonQuery();
                    MessageBox.Show("Поле оновлено успішно!");
                }
                else
                {
                    MessageBox.Show("Заповніть всі поля!");
                }
            }
            catch (Exception err)
            {
                MessageBox.Show($"Помилка при оновленні поля таблиці stores! {err.Message}");
            }
            finally
            {
                con.Close();
                storeTextBox.Clear();
                comNetworkTextBox.Clear();

            }
        }
    }
}
