﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Klemeshov_lab5
{
    public partial class FormApplication : Form
    {
        String sqlConnectionString = "Server=tcp:itacademy.database.windows.net,1433;Database=Klemeshov;User ID = Klemeshov;Password=Iszv12380;Trusted_Connection=False;Encrypt=True;";
        public FormApplication()
        {
            InitializeComponent();
        }


        private void FormApplication_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form ifrm = Application.OpenForms[0];
            ifrm.StartPosition = FormStartPosition.Manual;
            ifrm.Left = this.Left;
            ifrm.Top = this.Top;
            ifrm.Show();
        }

        private void FormApplication_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlConnectionString);
            con.Open();


            SqlCommand comm = new SqlCommand("SELECT [id] FROM [dbo].[workers]", con);

            SqlDataReader dr = comm.ExecuteReader();

            cmbWorkersApplicationsId.Items.Clear();
            while (dr.Read())
            {
                cmbWorkersApplicationsId.Items.Add(dr[0].ToString());
            }

            dr.Close();

            comm = new SqlCommand("SELECT [id] FROM [dbo].[courses]", con);

            dr = comm.ExecuteReader();

            cmbCoursesApplicationsId.Items.Clear();
            while (dr.Read())
            {
                cmbCoursesApplicationsId.Items.Add(dr[0].ToString());
            }

            dr.Close();

            comm = new SqlCommand("SELECT [id] FROM [dbo].[applications]", con);

            dr = comm.ExecuteReader();

            cmbApplications.Items.Clear();
            while (dr.Read())
            {
                cmbApplications.Items.Add(dr[0].ToString());
            }

            con.Close();
        }

        private void addApplication_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlConnectionString);
            try
            {

                con.Open();
                if (!String.IsNullOrWhiteSpace(fillOutDateTextBox.Text) && 
                    cmbWorkersApplicationsId.SelectedItem != null && 
                    cmbCoursesApplicationsId.SelectedItem != null) 
                { 
                    SqlCommand command = new SqlCommand($"INSERT INTO [dbo].[applications] ([worker_id], [course_id], [fill_out_date]) VALUES ({cmbWorkersApplicationsId.SelectedItem.ToString()}, {cmbCoursesApplicationsId.SelectedItem.ToString()}, '{fillOutDateTextBox.Text.ToString()}')", con);
                    command.ExecuteNonQuery();
                    MessageBox.Show("Заявку додано успішно!");

                    command = new SqlCommand("SELECT [id] FROM [dbo].[applications]", con);

                    SqlDataReader dr = command.ExecuteReader();

                    cmbApplications.Items.Clear();
                    while (dr.Read())
                    {
                        cmbApplications.Items.Add(dr[0].ToString());
                    }

                } 
                else
                {
                    MessageBox.Show("Заповніть всі поля і перевірте формат дати!");
                }
            }
            catch (Exception err)
            {
                MessageBox.Show($"Помилка при додаванні в таблицю applications! ({err.Message})");
            }
            finally
            {
                con.Close();
                fillOutDateTextBox.Clear();
                cmbWorkersApplicationsId.SelectedIndex = -1;
                cmbCoursesApplicationsId.SelectedIndex = -1;
            }
        }

        private void showApplications_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlConnectionString);
            con.Open();

            SqlDataAdapter ad = new SqlDataAdapter($"SELECT * FROM [dbo].[applications]", con);
            DataSet ds = new DataSet();
            ad.Fill(ds, "Applications");
            dgvApplications.DataSource = ds.Tables["Applications"];
            dgvApplications.Refresh();
            con.Close();
        }

        private void updateApplication_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlConnectionString);
            try
            {

                con.Open();
                if (!String.IsNullOrWhiteSpace(fillOutDateTextBox.Text) &&
                    cmbWorkersApplicationsId.SelectedItem != null &&
                    cmbCoursesApplicationsId.SelectedItem != null &&
                    cmbApplications.SelectedItem != null &&
                    cmbApplications.SelectedItem != null)
                {
                    SqlCommand command = new SqlCommand($"UPDATE [dbo].[applications] SET [worker_id] = {cmbWorkersApplicationsId.SelectedItem.ToString()}, [course_id] = {cmbCoursesApplicationsId.SelectedItem.ToString()}, [fill_out_date] = '{fillOutDateTextBox.Text.ToString()}' WHERE [id] = {cmbApplications.SelectedItem.ToString()}", con);
                    command.ExecuteNonQuery();
                    MessageBox.Show("Поле оновлено успішно!");
                } else
                {
                    MessageBox.Show("Заповніть всі поля і перевірте формат дати!");
                }
            }
            catch (Exception err)
            {
                MessageBox.Show($"Помилка при оновленні поля таблиці applications! ({err.Message})");
            }
            finally
            {
                con.Close();
                fillOutDateTextBox.Clear();
                cmbWorkersApplicationsId.SelectedIndex = -1;
                cmbCoursesApplicationsId.SelectedIndex = -1;
            }
        }


    }
}
